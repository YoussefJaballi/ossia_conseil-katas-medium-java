package com.ossia.kata.medium;

import org.junit.jupiter.api.Test;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;

import com.ossia.kata.medium.entity.Pet;
import com.ossia.kata.medium.entity.Shop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

public class JpaTest {

    @Test
    public void loadEntityModel() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("petshop");
        EntityManager em = emf.createEntityManager();

        try {
            Shop shop = em.find(Shop.class, 1L); 

            assertNotNull(shop);

            assertEquals("Nom du magasin", shop.getName());
            assertEquals(12345, shop.getRegistrationNumber());
            
            List<Pet> pets = shop.getPets();
            assertNotNull(pets);
            assertEquals(2, pets.size());
            

        } finally {
            em.close();
            emf.close();
        }
    }

    @Test
    public void addPets() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("petshop");
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();

            Shop shop = new Shop();
            shop.setName("PetShop");
            shop.setRegistrationNumber(12345);

            Pet pet1 = new Pet();
            pet1.setShop(shop);
            Pet pet2 = new Pet();
            pet2.setShop(shop);

            shop.getPets().add(pet1);
            shop.getPets().add(pet2);

            em.persist(shop);
            em.persist(pet1);
            em.persist(pet2);

            transaction.commit();

            Shop loadedShop = em.find(Shop.class, shop.getId());
            assertNotNull(loadedShop);
            List<Pet> pets = loadedShop.getPets();
            assertNotNull(pets);
            assertEquals(2, pets.size());

        } catch (Exception e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            throw e;
        } finally {
            em.close();
            emf.close();
        }
    }
}