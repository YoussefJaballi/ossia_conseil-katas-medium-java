# Katas pour profil Medium

## Durée : 30min

## Démarche à suivre pour tous les katas

- ### TDD
- ### Tester et optimiser le plus possible

# StringCalculator

Le kata original est [ici](https://codingdojo.org/kata/StringCalculator/)

**But de l'exercice** : Créer une fonction "add" qui prend une chaîne en entrée et renvoie une chaîne :

- La méthode peut prendre 0, 1 à n nombres séparés par une virgule et renvoie leur somme
- Une chaîne vide renverra "0".

*Exemple d'entrées* : "", "1", "4,2", "1.1,2.2,3.8", "1.01,2.002"

**Pour aller plus loin** : permettre à la méthode "add" de gérer les nouvelles lignes comme séparateurs

- "1\n2,3" devrait retourner "6"

# JPA

**Questions** :

- Corriger
  le [modèle de données](src/main/java/com/ossia/kata/medium/entity) ([Pet](src/main/java/com/ossia/kata/medium/entity/Pet.java)
  et [Shop](src/main/java/com/ossia/kata/medium/entity/Shop.java))
- Ajouter une annotation pour que registrationNumber soit unique dans
  l'entité [Shop](src/main/java/com/ossia/kata/medium/entity/Shop.java)
- Ajouter une annotation pour que l'Id soit généré automatiquement
  (dans [Pet](src/main/java/com/ossia/kata/medium/entity/Pet.java)
  et [Shop](src/main/java/com/ossia/kata/medium/entity/Shop.java))
- Proposer une implémentation *simple* de equals et hashCode
  pour [Shop](src/main/java/com/ossia/kata/medium/entity/Shop.java)
- Écrire le test loadEntityModel dans [](src/test/java/com/ossia/kata/medium/JpaTest.java) pour tester le chargement du
  modèle de données
- Écrire le test addPets qui effectue les actions suivantes :
    1. Ouvrir une session (EntityManager)
    2. Créer un Shop de name "PetShop"
    3. Créer deux Pets liés à "PetShop"
    4. Fermer la session
    5. Vérifier (assertions) qu'on a deux Pets liés à PetShop
